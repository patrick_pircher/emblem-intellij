package com.emblem;

import com.intellij.openapi.fileTypes.FileTypeConsumer;
import com.intellij.openapi.fileTypes.FileTypeFactory;
import org.jetbrains.annotations.NotNull;

public class EmblemFileTypeFactory extends FileTypeFactory {
    @Override
    public void createFileTypes(@NotNull FileTypeConsumer fileTypeConsumer) {
        System.out.println("createFileType");
        fileTypeConsumer.consume(EmblemFileType.INSTANCE, "emblem");
    }
}

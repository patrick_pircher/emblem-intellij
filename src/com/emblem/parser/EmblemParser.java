// This is a generated file. Not intended for manual editing.
package com.emblem.parser;

import com.intellij.lang.PsiBuilder;
import com.intellij.lang.PsiBuilder.Marker;
import static com.emblem.psi.EmblemTypes.*;
import static com.emblem.parser.EmblemParserUtil.*;
import com.intellij.psi.tree.IElementType;
import com.intellij.lang.ASTNode;
import com.intellij.psi.tree.TokenSet;
import com.intellij.lang.PsiParser;

@SuppressWarnings({"SimplifiableIfStatement", "UnusedAssignment"})
public class EmblemParser implements PsiParser {

  public ASTNode parse(IElementType t, PsiBuilder b) {
    parseLight(t, b);
    return b.getTreeBuilt();
  }

  public void parseLight(IElementType t, PsiBuilder b) {
    boolean r;
    b = adapt_builder_(t, b, this, null);
    Marker m = enter_section_(b, 0, _COLLAPSE_, null);
    if (t == CLASS_OR_ID) {
      r = class_or_id(b, 0);
    }
    else if (t == EXPR) {
      r = expr(b, 0);
    }
    else if (t == FIRST_WORD) {
      r = first_word(b, 0);
    }
    else if (t == LINE) {
      r = line(b, 0);
    }
    else if (t == PROP_VALUE) {
      r = prop_value(b, 0);
    }
    else if (t == ROOT_ITEM) {
      r = root_item(b, 0);
    }
    else {
      r = parse_root_(t, b, 0);
    }
    exit_section_(b, 0, m, t, r, true, TRUE_CONDITION);
  }

  protected boolean parse_root_(IElementType t, PsiBuilder b, int l) {
    return root(b, l + 1);
  }

  /* ********************************************************** */
  // ('#'cssid) | ('.'cssclass)
  public static boolean class_or_id(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "class_or_id")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, "<class or id>");
    r = class_or_id_0(b, l + 1);
    if (!r) r = class_or_id_1(b, l + 1);
    exit_section_(b, l, m, CLASS_OR_ID, r, false, null);
    return r;
  }

  // '#'cssid
  private static boolean class_or_id_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "class_or_id_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, "#");
    r = r && consumeToken(b, CSSID);
    exit_section_(b, m, null, r);
    return r;
  }

  // '.'cssclass
  private static boolean class_or_id_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "class_or_id_1")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, ".");
    r = r && consumeToken(b, CSSCLASS);
    exit_section_(b, m, null, r);
    return r;
  }

  /* ********************************************************** */
  // class_or_id ? (' ' | crlf) prop_value * text?
  public static boolean expr(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "expr")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, "<expression>");
    r = expr_0(b, l + 1);
    r = r && expr_1(b, l + 1);
    r = r && expr_2(b, l + 1);
    r = r && expr_3(b, l + 1);
    exit_section_(b, l, m, EXPR, r, false, null);
    return r;
  }

  // class_or_id ?
  private static boolean expr_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "expr_0")) return false;
    class_or_id(b, l + 1);
    return true;
  }

  // ' ' | crlf
  private static boolean expr_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "expr_1")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, " ");
    if (!r) r = consumeToken(b, CRLF);
    exit_section_(b, m, null, r);
    return r;
  }

  // prop_value *
  private static boolean expr_2(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "expr_2")) return false;
    int c = current_position_(b);
    while (true) {
      if (!prop_value(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "expr_2", c)) break;
      c = current_position_(b);
    }
    return true;
  }

  // text?
  private static boolean expr_3(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "expr_3")) return false;
    consumeToken(b, TEXT);
    return true;
  }

  /* ********************************************************** */
  // ("= "|"== "|"%") ? (tag|helper|'|')
  public static boolean first_word(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "first_word")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, "<first word>");
    r = first_word_0(b, l + 1);
    r = r && first_word_1(b, l + 1);
    exit_section_(b, l, m, FIRST_WORD, r, false, null);
    return r;
  }

  // ("= "|"== "|"%") ?
  private static boolean first_word_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "first_word_0")) return false;
    first_word_0_0(b, l + 1);
    return true;
  }

  // "= "|"== "|"%"
  private static boolean first_word_0_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "first_word_0_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, "= ");
    if (!r) r = consumeToken(b, "== ");
    if (!r) r = consumeToken(b, "%");
    exit_section_(b, m, null, r);
    return r;
  }

  // tag|helper|'|'
  private static boolean first_word_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "first_word_1")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, TAG);
    if (!r) r = consumeToken(b, HELPER);
    if (!r) r = consumeToken(b, "|");
    exit_section_(b, m, null, r);
    return r;
  }

  /* ********************************************************** */
  // first_word ' ' expr
  public static boolean line(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "line")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, "<line>");
    r = first_word(b, l + 1);
    r = r && consumeToken(b, " ");
    r = r && expr(b, l + 1);
    exit_section_(b, l, m, LINE, r, false, null);
    return r;
  }

  /* ********************************************************** */
  // property '=' (variable|string)
  public static boolean prop_value(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "prop_value")) return false;
    if (!nextTokenIs(b, PROPERTY)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, PROPERTY);
    r = r && consumeToken(b, SEPARATOR);
    r = r && prop_value_2(b, l + 1);
    exit_section_(b, m, PROP_VALUE, r);
    return r;
  }

  // variable|string
  private static boolean prop_value_2(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "prop_value_2")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, VARIABLE);
    if (!r) r = consumeToken(b, STRING);
    exit_section_(b, m, null, r);
    return r;
  }

  /* ********************************************************** */
  // !(crlf|':')
  static boolean property_recover(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "property_recover")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NOT_, null);
    r = !property_recover_0(b, l + 1);
    exit_section_(b, l, m, null, r, false, null);
    return r;
  }

  // crlf|':'
  private static boolean property_recover_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "property_recover_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, CRLF);
    if (!r) r = consumeToken(b, ":");
    exit_section_(b, m, null, r);
    return r;
  }

  /* ********************************************************** */
  // root_item *
  static boolean root(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "root")) return false;
    int c = current_position_(b);
    while (true) {
      if (!root_item(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "root", c)) break;
      c = current_position_(b);
    }
    return true;
  }

  /* ********************************************************** */
  // line crlf ?
  public static boolean root_item(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "root_item")) return false;
    boolean r, p;
    Marker m = enter_section_(b, l, _NONE_, "<root item>");
    r = line(b, l + 1);
    p = r; // pin = 1
    r = r && root_item_1(b, l + 1);
    exit_section_(b, l, m, ROOT_ITEM, r, p, property_recover_parser_);
    return r || p;
  }

  // crlf ?
  private static boolean root_item_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "root_item_1")) return false;
    consumeToken(b, CRLF);
    return true;
  }

  final static Parser property_recover_parser_ = new Parser() {
    public boolean parse(PsiBuilder b, int l) {
      return property_recover(b, l + 1);
    }
  };
}

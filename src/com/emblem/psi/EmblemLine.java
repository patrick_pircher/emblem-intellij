// This is a generated file. Not intended for manual editing.
package com.emblem.psi;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElement;

public interface EmblemLine extends PsiElement {

  @NotNull
  EmblemExpr getExpr();

  @NotNull
  EmblemFirstWord getFirstWord();

}

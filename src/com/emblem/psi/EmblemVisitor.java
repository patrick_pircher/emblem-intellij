// This is a generated file. Not intended for manual editing.
package com.emblem.psi;

import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.PsiElement;

public class EmblemVisitor extends PsiElementVisitor {

  public void visitClassOrId(@NotNull EmblemClassOrId o) {
    visitPsiElement(o);
  }

  public void visitExpr(@NotNull EmblemExpr o) {
    visitPsiElement(o);
  }

  public void visitFirstWord(@NotNull EmblemFirstWord o) {
    visitPsiElement(o);
  }

  public void visitLine(@NotNull EmblemLine o) {
    visitPsiElement(o);
  }

  public void visitPropValue(@NotNull EmblemPropValue o) {
    visitPsiElement(o);
  }

  public void visitRootItem(@NotNull EmblemRootItem o) {
    visitPsiElement(o);
  }

  public void visitPsiElement(@NotNull PsiElement o) {
    visitElement(o);
  }

}

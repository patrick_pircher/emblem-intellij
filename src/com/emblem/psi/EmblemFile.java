package com.emblem.psi;

import com.emblem.EmblemFileType;
import com.emblem.EmblemLanguage;
import com.intellij.extapi.psi.PsiFileBase;
import com.intellij.openapi.fileTypes.FileType;
import com.intellij.psi.FileViewProvider;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;

public class EmblemFile extends PsiFileBase {
    public EmblemFile(@NotNull FileViewProvider viewProvider) {
        super(viewProvider, EmblemLanguage.INSTANCE);
    }

    @NotNull
    @Override
    public FileType getFileType() {
        return EmblemFileType.INSTANCE;
    }

    @Override
    public String toString() {
        return "Emblem File";
    }

    @Override
    public Icon getIcon(int flags) {
        return super.getIcon(flags);
    }
}
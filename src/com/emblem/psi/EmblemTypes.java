// This is a generated file. Not intended for manual editing.
package com.emblem.psi;

import com.intellij.psi.tree.IElementType;
import com.intellij.psi.PsiElement;
import com.intellij.lang.ASTNode;
import com.emblem.psi.impl.*;

public interface EmblemTypes {

  IElementType CLASS_OR_ID = new EmblemElementType("CLASS_OR_ID");
  IElementType EXPR = new EmblemElementType("EXPR");
  IElementType FIRST_WORD = new EmblemElementType("FIRST_WORD");
  IElementType LINE = new EmblemElementType("LINE");
  IElementType PROP_VALUE = new EmblemElementType("PROP_VALUE");
  IElementType ROOT_ITEM = new EmblemElementType("ROOT_ITEM");

  IElementType COMMENT = new EmblemTokenType("comment");
  IElementType CRLF = new EmblemTokenType("crlf");
  IElementType CSSCLASS = new EmblemTokenType("cssclass");
  IElementType CSSID = new EmblemTokenType("cssid");
  IElementType HELPER = new EmblemTokenType("helper");
  IElementType OPERATOR = new EmblemTokenType("operator");
  IElementType PROPERTY = new EmblemTokenType("property");
  IElementType SEPARATOR = new EmblemTokenType("=");
  IElementType STRING = new EmblemTokenType("string");
  IElementType TAG = new EmblemTokenType("tag");
  IElementType TEXT = new EmblemTokenType("text");
  IElementType VARIABLE = new EmblemTokenType("variable");

  class Factory {
    public static PsiElement createElement(ASTNode node) {
      IElementType type = node.getElementType();
       if (type == CLASS_OR_ID) {
        return new EmblemClassOrIdImpl(node);
      }
      else if (type == EXPR) {
        return new EmblemExprImpl(node);
      }
      else if (type == FIRST_WORD) {
        return new EmblemFirstWordImpl(node);
      }
      else if (type == LINE) {
        return new EmblemLineImpl(node);
      }
      else if (type == PROP_VALUE) {
        return new EmblemPropValueImpl(node);
      }
      else if (type == ROOT_ITEM) {
        return new EmblemRootItemImpl(node);
      }
      throw new AssertionError("Unknown element type: " + type);
    }
  }
}

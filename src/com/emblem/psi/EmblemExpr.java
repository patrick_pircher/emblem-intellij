// This is a generated file. Not intended for manual editing.
package com.emblem.psi;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElement;

public interface EmblemExpr extends PsiElement {

  @Nullable
  EmblemClassOrId getClassOrId();

  @NotNull
  List<EmblemPropValue> getPropValueList();

  @Nullable
  PsiElement getCrlf();

}

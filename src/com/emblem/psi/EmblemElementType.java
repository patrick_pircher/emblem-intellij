package com.emblem.psi;

import com.emblem.EmblemLanguage;
import com.intellij.psi.tree.IElementType;
import org.jetbrains.annotations.NonNls;
import org.jetbrains.annotations.NotNull;

public class EmblemElementType extends IElementType {
    public EmblemElementType(@NotNull @NonNls String debugName) {
        super(debugName, EmblemLanguage.INSTANCE);
    }
}

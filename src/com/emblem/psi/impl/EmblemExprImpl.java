// This is a generated file. Not intended for manual editing.
package com.emblem.psi.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static com.emblem.psi.EmblemTypes.*;
import com.intellij.extapi.psi.ASTWrapperPsiElement;
import com.emblem.psi.*;

public class EmblemExprImpl extends ASTWrapperPsiElement implements EmblemExpr {

  public EmblemExprImpl(ASTNode node) {
    super(node);
  }

  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof EmblemVisitor) ((EmblemVisitor)visitor).visitExpr(this);
    else super.accept(visitor);
  }

  @Override
  @Nullable
  public EmblemClassOrId getClassOrId() {
    return findChildByClass(EmblemClassOrId.class);
  }

  @Override
  @NotNull
  public List<EmblemPropValue> getPropValueList() {
    return PsiTreeUtil.getChildrenOfTypeAsList(this, EmblemPropValue.class);
  }

  @Override
  @Nullable
  public PsiElement getCrlf() {
    return findChildByType(CRLF);
  }

}

// This is a generated file. Not intended for manual editing.
package com.emblem.psi.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static com.emblem.psi.EmblemTypes.*;
import com.intellij.extapi.psi.ASTWrapperPsiElement;
import com.emblem.psi.*;

public class EmblemPropValueImpl extends ASTWrapperPsiElement implements EmblemPropValue {

  public EmblemPropValueImpl(ASTNode node) {
    super(node);
  }

  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof EmblemVisitor) ((EmblemVisitor)visitor).visitPropValue(this);
    else super.accept(visitor);
  }

  @Override
  @NotNull
  public PsiElement getProperty() {
    return findNotNullChildByType(PROPERTY);
  }

  @Override
  @Nullable
  public PsiElement getString() {
    return findChildByType(STRING);
  }

  @Override
  @Nullable
  public PsiElement getVariable() {
    return findChildByType(VARIABLE);
  }

}

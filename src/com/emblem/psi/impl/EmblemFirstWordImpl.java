// This is a generated file. Not intended for manual editing.
package com.emblem.psi.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static com.emblem.psi.EmblemTypes.*;
import com.intellij.extapi.psi.ASTWrapperPsiElement;
import com.emblem.psi.*;

public class EmblemFirstWordImpl extends ASTWrapperPsiElement implements EmblemFirstWord {

  public EmblemFirstWordImpl(ASTNode node) {
    super(node);
  }

  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof EmblemVisitor) ((EmblemVisitor)visitor).visitFirstWord(this);
    else super.accept(visitor);
  }

  @Override
  @Nullable
  public PsiElement getHelper() {
    return findChildByType(HELPER);
  }

  @Override
  @Nullable
  public PsiElement getTag() {
    return findChildByType(TAG);
  }

}

// This is a generated file. Not intended for manual editing.
package com.emblem.psi.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static com.emblem.psi.EmblemTypes.*;
import com.intellij.extapi.psi.ASTWrapperPsiElement;
import com.emblem.psi.*;

public class EmblemLineImpl extends ASTWrapperPsiElement implements EmblemLine {

  public EmblemLineImpl(ASTNode node) {
    super(node);
  }

  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof EmblemVisitor) ((EmblemVisitor)visitor).visitLine(this);
    else super.accept(visitor);
  }

  @Override
  @NotNull
  public EmblemExpr getExpr() {
    return findNotNullChildByClass(EmblemExpr.class);
  }

  @Override
  @NotNull
  public EmblemFirstWord getFirstWord() {
    return findNotNullChildByClass(EmblemFirstWord.class);
  }

}

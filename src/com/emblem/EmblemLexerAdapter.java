package com.emblem;

import com.intellij.lexer.FlexAdapter;

import java.io.Reader;

/**
 * Created by patrick on 13-08-2014.
 */
public class EmblemLexerAdapter extends FlexAdapter {
    public EmblemLexerAdapter() {
        super(new EmblemLexer((Reader) null));
    }
}

package com.emblem;

import com.emblem.psi.EmblemTypes;
import com.intellij.lexer.FlexAdapter;
import com.intellij.lexer.Lexer;
import com.intellij.openapi.editor.colors.TextAttributesKey;
import com.intellij.openapi.fileTypes.SyntaxHighlighterBase;
import com.intellij.psi.TokenType;
import com.intellij.psi.tree.IElementType;
import org.jetbrains.annotations.NotNull;

import java.io.Reader;
import java.util.HashMap;
import java.util.Map;

import static com.intellij.openapi.editor.colors.TextAttributesKey.createTextAttributesKey;

public class EmblemSyntaxHighlighter extends SyntaxHighlighterBase {
    public static final TextAttributesKey SEPARATOR = createTextAttributesKey("EMBLEM_SEPARATOR");
    public static final TextAttributesKey HELPER = createTextAttributesKey("EMBLEM_HELPER");
    public static final TextAttributesKey COMMENT = createTextAttributesKey("EMBLEM_COMMENT");
    public static final TextAttributesKey STRING = createTextAttributesKey("EMBLEM_STRING");
    public static final TextAttributesKey VARIABLE = createTextAttributesKey("EMBLEM_VARIABLE");
    public static final TextAttributesKey PROPERTY = createTextAttributesKey("EMBLEM_PROPERTY");
    public static final TextAttributesKey OPERATOR = createTextAttributesKey("EMBLEM_OPERATOR");
    public static final TextAttributesKey TEXT = createTextAttributesKey("EMBLEM_TEXT");
    public static final TextAttributesKey CSSCLASS = createTextAttributesKey("EMBLEM_CSSCLASS");
    public static final TextAttributesKey CSSID = createTextAttributesKey("EMBLEM_CSSID");

    public static final TextAttributesKey TAG = createTextAttributesKey("EMBLEM_TAG");

    static final TextAttributesKey BAD_CHARACTER = createTextAttributesKey("EMBLEM_BAD_CHARACTER");

    protected static final Map<IElementType, TextAttributesKey> ATTRIBUTES = new HashMap<IElementType, TextAttributesKey>();
    static {
        fillMap(ATTRIBUTES, SEPARATOR, EmblemTypes.SEPARATOR);
        fillMap(ATTRIBUTES, COMMENT, EmblemTypes.COMMENT);
        fillMap(ATTRIBUTES, OPERATOR, EmblemTypes.OPERATOR);
        fillMap(ATTRIBUTES, PROPERTY, EmblemTypes.PROPERTY);
        fillMap(ATTRIBUTES, BAD_CHARACTER, TokenType.BAD_CHARACTER);
        fillMap(ATTRIBUTES, TAG, EmblemTypes.TAG);
        fillMap(ATTRIBUTES, STRING, EmblemTypes.STRING);
        fillMap(ATTRIBUTES, VARIABLE, EmblemTypes.VARIABLE);
        fillMap(ATTRIBUTES, TEXT, EmblemTypes.TEXT);
        fillMap(ATTRIBUTES, HELPER, EmblemTypes.HELPER);
        fillMap(ATTRIBUTES, CSSCLASS, EmblemTypes.CSSCLASS);
        fillMap(ATTRIBUTES, CSSID, EmblemTypes.CSSID);
    }

    @NotNull
    @Override
    public Lexer getHighlightingLexer() {
        return new FlexAdapter(new EmblemLexer((Reader) null));
    }

    @NotNull
    @Override
    public TextAttributesKey[] getTokenHighlights(IElementType tokenType) {
        return pack(ATTRIBUTES.get(tokenType));
    }
}

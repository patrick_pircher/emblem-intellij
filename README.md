# Intellij Idea Plugin for emblem.js#

This project aims to create a plugin for[ emblem.js](http://emblemjs.com/) .
Currently it only supports basic syntax highlighting.

Plugin page on JetBrains repo: https://plugins.jetbrains.com/plugin/7628

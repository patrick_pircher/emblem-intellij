# Emblem Changelog

### 0.1.1 (10 January, 2015)
* multiline syntax highlighting
* syntax highlighting for helpers inside {{}}
* disabled parser definition
* added default syntax colors

### 0.1
* Basic Syntax highlighting